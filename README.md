# Parser For `Amazon.com`


# Work plan
1. Build a website										\| +
	1. Install usefull apps on ubuntu 20.04				\| +
	2. Setting up installed apps						\| +
	3. Split page ui to blocks							\| +
	4. Code ( HTML, CSS, JS 'Native' )					\| +
	5. Connect bank data's								\| *



# Links



# Requirement apps to edit 
1. Sublime text \*.										\| +
2. Python 3.7+											\| +




# Requirement apps to view site in pc
1. Python 3.7+											\| +


# Installation commands (**Ubuntu 20.04**)
1. python3					`sudo apt  install python3 python3-pip`
2. sublime text				`sudo snap install sublime-text`
3. git						`sudo apt  install git`
4. packages					`pip3 install -r requirements.txt`


# Hierarchy
```
.
├── LICENSE
├── manage.py
├── parser
│	├── errors.py
│	├── __init__.py
│	└── parser.py
├── README.md
└── requirements.txt

1 directories, 7 files
```

# Info 

for using this app you need to download chromedirivver from official site `https://chromedriver.chromium.org/downloads`

for runing script use `./manage.py`

