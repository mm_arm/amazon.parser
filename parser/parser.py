from selenium import webdriver
from bs4 import BeautifulSoup as soup


class AmazonBlockError(Exception):
    """ Error for handling Amazon capcha blocking"""
    pass


class Amazon:
    """
        Parser for amazon.com
    """
    def __init__(
                self,
                driver_path: str,
                base_url: str = "http://amazon.com",
                render_timeout: int = 1,
        ) -> None:
        super().__init__()
        self.url = base_url
        self.render_timeout = render_timeout
        self.session = webdriver.Chrome(driver_path)


    def load(self, url: str) -> webdriver.Chrome:
        self.session.get(url)
        return self.session


    def query(self, data: str) -> str:
        data_ = ''
        for word in data.split():
            data_ += f'{word}+'
        return data_


    def search(
                self,
                item: str,
                min_price: int = 1,
                max_price: int = 1000,
                page_id: int = 1
        ) -> list:
        request_url = (
            f'{self.url}/s?k={self.query(item)}&'
            f'rh=p_36%3A{min_price}00-{max_price}00&page={page_id}'
        )

        self.load(request_url)

        data = self.session.page_source

        page_soup = soup(data, 'html.parser')

        items = page_soup.select('.s-main-slot.s-result-list > .s-result-item')


        return items


    def check_block(self, page: soup) -> bool:
        return len(page.select('#add-to-cart-button')) != 1


    def get_info(self, product: soup, debug:bool=False, **load_kwargs: dict) -> dict:
        block = False
        result = {
            "descriptions":None,
            "weight":None,
            "price":None,
            "image_link":None
        }

        try:
            half_link = product.select('div > h2 > a[href]')[0]['href']
        except:
            return result

        result["link"] = self.url+half_link
        result["name"] = product.select('div > h2 > a > span')[0].text

        # Loading Main Page
        self.load(result['link'], **load_kwargs)
        page_soup = soup(self.session.page_source, 'html.parser')

        if self.check_block(page_soup):
            block = True
            if debug:
                raise AmazonBlockError(
                    (
                        "Please try again, Amazon was blocked"
                        " you and want to solve capcha"
                    )
                )
        if not block:

            item_selector = '#productDescription > p'
            price_selector = '#priceblock_ourprice'
            weights_selector = '#comparison_other_attribute_row > td'
 
            result["descriptions"] = ' '.join(
                [item.text for item in page_soup.select(item_selector)]
            )
 
            result["price"] = ''.join(
                [item.text for item in page_soup.select(price_selector)]
            )
 
            images = page_soup.find_all('img', src=True, id='landingImage')
            result["image_link"] = ', '.join([img['src'] for img in images])
 
            weights = page_soup.select(weights_selector)
            result['weight'] = ' '.join(
                [elem.text for elem in weights]
            )

        return result
