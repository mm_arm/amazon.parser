#!/usr/bin/env python3
from parser.parser import Amazon, AmazonBlockError
import csv

DRIVER = './depends/chromedriver'
CSV_FILE_NAME = 'output.csv'
FIELDS = ['link', 'name', 'descriptions', 'image_link', 'price', 'weight']
PAGE_COUNT = 20

if __name__ == "__main__":
    PARSER = Amazon(DRIVER)
    with open(CSV_FILE_NAME, 'w') as fo:
        WRIER = csv.DictWriter(fo, FIELDS)
        WRIER.writeheader()
        for page_id in range(1, PAGE_COUNT+1):
            for product in PARSER.search("telescope", 60, 1000, page_id):
                try:
                    WRIER.writerow(PARSER.get_info(product))
                except AmazonBlockError as error:
                    print(f"EXIT {error}")
                    break
